<?php


namespace Eiprice\Eipthreads\Traits;


trait Singleton
{
    /**
     * Store the singleton object.
     */
    private static $singleton = false;

    /**
     * Create an inaccessible contructor.
     */
    final private function __construct()
    {
        $this->__instance();
    }

    /**
     * Fetch an instance of the class.
     */
    public static function getInstance()
    {
        if (self::$singleton === false) {
            self::$singleton = new self();
        }

        return self::$singleton;
    }

    /**
     * Instead of a constructor, put your
     * initialisation code here.
     */
    public function __instance() {
        
    }
}
