<?php


namespace Eiprice\Eipthreads;

use Eiprice\Eipthreads\Interfaces\IEirunner;

/**
 * Class Eirunner
 * @package Eiprice\Eipthreads
 */
class Eirunner 
    implements IEirunner
{
    private $pool = null;
    private $data = null;

    public function __construct(int $poolSize = 0, $autoload = null)
    {
        $this->data = new Eivolatile();
        if ( empty($autoload)){
            $autoload = '../../autoload.php';
        }
        $this->pool = new Eipool(
            $poolSize,
            Eiworker::class,
            [
                $autoload,
                $this->data
            ]
        );
    }

    /**
     * @param Eitask $task
     * @return IEirunner
     */
    public function addTask(Eitask $task): IEirunner
    {
        $this->pool->submit($task);

        return $this;
    }

    public function getData() : Eivolatile
    {
        return $this->data;
    }

    public function process(): array
    {
        $this->pool->process();

        return (array) $this->data;
    }
}
