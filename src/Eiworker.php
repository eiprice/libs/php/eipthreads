<?php


namespace Eiprice\Eipthreads;

use \Worker;
use \Volatile;

/**
 * Class Eiworker
 * @package Eiprice\Eipthreads
 */
class Eiworker extends Worker
{
    protected $loader;
    protected $data;

    /**
     * Eiworker constructor.
     * @param String $loader
     * @param Volatile $data
     */
    public function __construct(String $loader, Volatile $data)
    {
        $this->loader = $loader;
        $this->data = $data;
    }

    public function checkLaravel()
    {
        $path = dirname($this->loader) . "/laravel";

        return is_dir($path);
    }

    /**
     * 
     */
    public function run() : void
    {
        require_once $this->loader;
    }

    /**
     * @param int $options
     * @return bool
     */
    public function start($options = PTHREADS_INHERIT_ALL)
    {
        return parent::start(PTHREADS_INHERIT_NONE);
    }

    /**
     * @return Volatile
     */
    public function getData() : Volatile
    {
        return $this->data;
    }

    /**
     * @param $data
     */
    public function addData($data, $key = 'data') : void
    {
//        $obj_data = $this->data;
//        if ( !is_null($data)){
//            $obj_data[$key][] = $data;
//        }
    }
}
