<?php

namespace Eiprice\Eipthreads\Laravel;

use Illuminate\Foundation\Application as IlluminateApplication;

class Application extends IlluminateApplication
{
    /**
     * Get the path to the cached services.php file.
     *
     * @return string
     */
    public function getCachedServicesPath()
    {
        return env('APP_SERVICES_CACHE', storage_path('bootstrap/cache/services.php'));
    }


    /**
     * Get the path to the cached packages.php file.
     *
     * @return string
     */
    public function getCachedPackagesPath()
    {
        return env('APP_PACKAGES_CACHE', storage_path('bootstrap/cache/packages.php'));
    }


    /**
     * Get the path to the configuration cache file.
     *
     * @return string
     */
    public function getCachedConfigPath()
    {
        return env('APP_CONFIG_CACHE', storage_path('bootstrap/cache/config.php'));
    }


    /**
     * Get the path to the routes cache file.
     *
     * @return string
     */
    public function getCachedRoutesPath()
    {
        return env('APP_ROUTES_CACHE', storage_path('bootstrap/cache/routes.php'));
    }
}
