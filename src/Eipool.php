<?php


namespace Eiprice\Eipthreads;

use \Pool;

/**
 * Class Eipool
 * @package Eiprice\Eipthreads
 */
class Eipool extends Pool
{
    public function process(): void
    {
        while ($this->collect());

        $this->shutdown();
    }
}
