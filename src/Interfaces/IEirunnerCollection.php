<?php


namespace Eiprice\Eipthreads\Interfaces;


use Eiprice\Eipthreads\Eirunner;
use Eiprice\Eipthreads\Exceptions\{EirunnerAlreadyExists, EirunnerNotCreated};

/**
 * Interface IEirunnerCollection
 * @package Eiprice\Eipthreads\Interfaces
 */
interface IEirunnerCollection
{

    /**
     * @param $name
     * @return IEirunner
     */
    public function getEirunner($name, $poolSize, $autoload = ''): IEirunner;
}
