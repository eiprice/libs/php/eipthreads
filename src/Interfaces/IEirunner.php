<?php


namespace Eiprice\Eipthreads\Interfaces;


use Eiprice\Eipthreads\Eipool;
use Eiprice\Eipthreads\Eitask;
use Eiprice\Eipthreads\Eivolatile;
use Eiprice\Eipthreads\Eiworker;

/**
 * Interface IEirunner
 * @package Eiprice\Eipthreads\Interfaces
 */
interface IEirunner
{
    /**
     * IEirunner constructor.
     * @param int $poolSize
     * @param null $autoload
     */
    public function __construct(int $poolSize = 0, $autoload = null);


    /**
     * @param Eitask $task
     * @return IEirunner
     */
    public function addTask(Eitask $task): IEirunner;

    /**
     * @return Eivolatile
     */
    public function getData() : Eivolatile;

    /**
     * @return array
     */
    public function process(): array;
}
