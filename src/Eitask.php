<?php


namespace Eiprice\Eipthreads;

use Illuminate\Contracts\Foundation\Application;
use Eiprice\Eipthreads\Laravel\Application as EipthreadsApplication;
use Threaded;

/**
 * Class Eitask
 * @package Eiprice\Eipthreads
 */
abstract class Eitask extends Threaded
{
    /**
     * @var Eiworker
     */
    protected $worker;

    protected  $app;

    public function run() : void
    {
        global $app;

        if ( $this->worker->checkLaravel() ) {
            $app = new EipthreadsApplication(
                dirname(dirname($this->worker->loader))
            );

            $app->bind(
                \Eiprice\Eipthreads\Laravel\Contracts\ThreadKernel::class,
                \Eiprice\Eipthreads\Laravel\Thread\Kernel::class
            );

            $app->singleton(
                \Illuminate\Contracts\Debug\ExceptionHandler::class,
                \Eiprice\Eipthreads\Exceptions\Handler::class
            );



            $kernel = $app->make(\Eiprice\Eipthreads\Laravel\Contracts\ThreadKernel::class);

            $result = $kernel->handle(
                $this,
                new \Symfony\Component\Console\Output\ConsoleOutput
            );
        }

        // $result = $this->execute();
        $this->save($result);
    }

    protected function save($data) : void
    {
        $this->worker->addData($data);
    }

    abstract public function execute(Application $app) : ?array;
}
