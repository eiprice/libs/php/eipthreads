<?php


namespace Eiprice\Eipthreads\React;

use FastRoute\Dispatcher;
use FastRoute\Dispatcher\GroupCountBased;
use FastRoute\RouteCollector;
use LogicException;
use React\Http\Response;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
use Illuminate\View\View;


/**
 * Class Router
 * @package Eiprice\Eipthreads\React
 */
final class Router
{
    private $dispatcher;

    /**
     * @var callable|null
     */
    private $handler = null;

    /**
     * @var int|null
     */
    private $status = null;


    private $params = [];

    /**
     * @var ServerRequestInterface
     */
    private $request;

    public function __construct(RouteCollector $routes)
    {
        $this->dispatcher = new GroupCountBased($routes->getData());
    }

    private function setup(ServerRequestInterface $request)
    {
        //
        $this->request = $request;

        //
        $routeInfo = $this->dispatcher->dispatch($request->getMethod(), $request->getUri()->getPath());

        //
        $this->status = $routeInfo[0];

        //
        $this->handler = isset($routeInfo[1]) ? $routeInfo[1] : null;

        //
        $this->params = isset($routeInfo[2]) ?  $routeInfo[2] : null;
    }

    protected function generate_error_response($http_code, $content)
    {
        try{

            if ( view()->exists("error.{$http_code}") ){
                return $this->get_response(view("error.{$http_code}", ['error' => $content]));
            }
        } catch (\Exception $e){
            dd($e->getMessage());
        }



        return new Response($http_code, ['Content-Type' => 'text/plain'], $content);
    }

    private function generate_response($content, $http_code = 200, $contentType = 'text/plain')
    {
        if ( ! $this->check_ok($http_code) ){
            return $this->generate_error_response($http_code, $content);
        }


        return new Response($http_code, ['Content-Type' => $contentType], $content);
    }

    /**
     * @return bool
     */
    protected function check_ok($http_code)
    {
        if (filter_var($http_code, FILTER_VALIDATE_INT, array("options" => array("min_range"=> 200, "max_range"=> 299)))) {
            return true;
        } else {
            false;
        }
    }

    private function get_object_response($response)
    {
        switch (get_class($response)){
            /**
             * @var View $response
             */
            case 'Illuminate\View\View':
                $response = $this->generate_response($response->render(), 200, 'text/html');
        }

        return $response;
    }

    private function get_string_response($response)
    {
        return $this->generate_response($response);
    }

    private function get_response($response)
    {
        if ( is_string($response)){
            return $this->get_string_response($response);
        }

        if ( is_object($response)){
            return $this->get_object_response($response);
        }
    }


    private function call_handler()
    {
        $response = ($this->handler)($this->request, ... array_values($this->params));


        if ( ! ($response instanceof ResponseInterface)) {

            $response = $this->get_response($response);

        }


        return  $response;
    }






    public function __invoke(ServerRequestInterface $request)
    {
        try{
            $this->setup($request);


            switch ($this->status) {
                case Dispatcher::NOT_FOUND:
                    return $this->generate_response('Not found', 404 );
                case Dispatcher::METHOD_NOT_ALLOWED:
                    return  $this->generate_response('Method not allowed', 405 );
                case Dispatcher::FOUND:
                    return $this->call_handler();
            }

        } catch (\Exception $e){
            return $this->generate_response($e, 500 );
        }

        throw new LogicException('Something wrong with routing');
    }
}
