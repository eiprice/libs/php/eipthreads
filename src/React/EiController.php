<?php

namespace Eiprice\Eipthreads\React;

use Eiprice\Eipthreads\EirunnerCollection;
use \Eiprice\Eipthreads\Interfaces\IEirunnerCollection;

use Psr\Log\LoggerInterface;
use React\Http\Response;

use \ReflectionClass, \ReflectionMethod;

/**
 * Class EiController
 * @package Eiprice\Eipthreads\React
 */
abstract class EiController
{
    /**
     * @var IEirunnerCollection
     */
    protected $collection;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * EiController constructor.
     * @param LoggerInterface LoggerInterface
     */
    public function __construct(LoggerInterface $logger)
    {
        $collection = EirunnerCollection::getInstance();

        $this->collection = $collection;

        $this->logger = $logger;
    }


    protected function getParam(String $text)
    {
        $pattern = "/^@(\w+)\s+(.*)$/i";

        preg_match($pattern, $text, $matches);


        return [ $matches[1] => $matches[2] ];
    }

    /**
     * @param ReflectionMethod $method
     * @return array|null
     */
    protected function getRoute(ReflectionMethod $method)
    {
        $comment_string = $method->getDocComment();

        $pattern = "#(@[a-zA-Z]+\s*[a-zA-Z0-9, ()_].*)#";

        preg_match_all($pattern, $comment_string, $matches, PREG_PATTERN_ORDER);

        $http_method = "get";
        $route = null;



        foreach ($matches as $match){
            if ( empty($match)){
                continue;
            }
            foreach ($match as $text){
                $parse = $this->getParam($text);
                if ( isset($parse['method'])){
                    $http_method = $parse['method'];
                }

                if ( isset($parse['route'])){
                    $route = $parse['route'];
                }
            }
        }

        if ( ! is_null($route)){
            return [
                'method' => $http_method,
                'route' => $route,
            ];
        } else {
            return null;
        }
    }

    /**
     * @return array
     * @throws \ReflectionException
     */
    public function getRoutes()
    {
        $result = [];

        $reflection = new ReflectionClass($this);
        foreach ($reflection->getMethods() as $method){
            if ( $route = $this->getRoute($method)){
                $route['handler'] = [$this, $method->name];
                $result[] = $route;
            }
        }

        return $result;
    }
}
