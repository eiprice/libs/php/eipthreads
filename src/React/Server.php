<?php


namespace Eiprice\Eipthreads\React;

use FastRoute\DataGenerator\GroupCountBased;
use FastRoute\RouteCollector;
use FastRoute\RouteParser\Std;
use Psr\Log\LoggerInterface;
use React\EventLoop\Factory as EventLoopFactory;
use React\Http\Response;
use React\Http\Server as ReactHttpServer;
use React\Socket\Server as ReactSockerServer;
use \Throwable;

/**
 * Class Server
 * @package Eiprice\Eipthreads\React
 */
class Server
{
    protected $controllers = [];


    /**
     * @var LoggerInterface
     */
    protected $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public function addController($controller, array $parameters = [])
    {
        if ( is_object($controller)){
            $this->controllers[] = $controller;
        }

        if ( is_string($controller)){
            $this->controllers[] = app()->make($controller, $parameters);
        }
    }


    protected function getRouterCollector() : RouteCollector
    {
        $routes = new RouteCollector(new Std(), new GroupCountBased());

        foreach ($this->controllers as $controller){
            /**
             * @var EiController $controller
             */
            $controller_routes = $controller->getRoutes();



            foreach ($controller_routes as $controller_route){

                $method = strtoupper($controller_route['method']);
                $this->logger->debug("Registering: {$method} {$controller_route['route']}");

                $routes->addRoute(
                    $method,
                    $controller_route['route'],
                    $controller_route['handler']
                );
            }
        }

        return $routes;
    }

    public function run($bind = '0.0.0.0', $port = '8080')
    {
        $loop = EventLoopFactory::create();

        $server = new ReactHttpServer(
            new Router(
                $this->getRouterCollector()
            )
        );

        $server->on('error', function (Throwable $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());

            return new Response(500, [], 'Interal Server Error');
        });

        $socket = new ReactSockerServer("{$bind}:{$port}", $loop);
        $server->listen($socket);


        $this->logger->info('Listening on ' . str_replace('tcp:', 'http:', $socket->getAddress()));

        $loop->run();
    }
}
