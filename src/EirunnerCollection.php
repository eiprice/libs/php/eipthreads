<?php


namespace Eiprice\Eipthreads;

use Eiprice\Eipthreads\Interfaces\{IEirunner, IEirunnerCollection};
use Eiprice\Eipthreads\Exceptions\{EirunnerAlreadyExists, EirunnerNotCreated};

use Eiprice\Eipthreads\Traits\Singleton;

/**
 * Class EirunnerCollection
 * @package Eiprice\Eipthreads
 */
class EirunnerCollection
    implements IEirunnerCollection
{
    use Singleton;

    protected static $default_autoload = __DIR__ . '/../../../autoload.php';
    
    /**
     * @var array
     */
    protected $collection;
    
    
    public function getEirunner($name, $poolSize, $autoload = '') : IEirunner
    {
        if ( empty($autoload)){
            $autoload = realpath(self::$default_autoload);
        }

        if ( isset($this->collection[$name])){
            return $this->collection[$name]; 
        } else {
            return $this->collection[$name] = new Eirunner($poolSize, $autoload);
        }
    }
}
